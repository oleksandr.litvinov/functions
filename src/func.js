const getSum = (str1, str2) => {
  if (typeof str1 !== 'string' && typeof str2 !== 'string') {
    return false
  }
  if (str1.match(/^\d+$/) !== null && str2.match(/^\d+$/) !== null) {
    return String(+str1 + (+str2))
  }
  if (str1.match(/^\d+$/) === null && str2.match(/^\d+$/) === null) {
    return false
  }
  if (str1 === '') {
    return str2
  }
  if (str2 === '') {
    return str1
  }
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let amountOfPosts = 0
  let amountOfComments = 0
  listOfPosts.forEach(post => {
    if (post.author === authorName) {
      amountOfPosts++
    }
    if (post.comments) {
      post.comments.forEach(cur => {
        if (cur.author === authorName) {
          amountOfComments++
        }
      })
    }
  })
  return `Post:${amountOfPosts},comments:${amountOfComments}`
};

const tickets = (people) => {
  const ticketCost = 25
  let change = 0
  for (let money of people) {
    if (money - ticketCost > change) {
      return 'NO'
    }
    change += money > ticketCost ? change - (change - ticketCost) : money
  }
  return 'YES'
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
